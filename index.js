const http = require('http');
const fs = require('fs');
const url = require('url');

const PORT = 8080;
const JSON_SPACES_NUM = 2;
const FILES_DIR_PATH = './file';
const LOGS_DIR_PATH = './logs';
const LOGS_FILE_PATH = `${LOGS_DIR_PATH}/logs.json`;
const STATUS_CODE = {
  OK: 200,
  BAD_REQUEST: 400,
  NOT_FOUND: 404
};

const logToFile = (message, filePath = LOGS_FILE_PATH) => {
  const dirPath = filePath.slice(0, filePath.indexOf('/', filePath.indexOf('/') + 1));

  fs.readFile(filePath, 'utf8', (err, content) => {
    if (err) {
      if (err.code === 'ENOENT') {
        fs.mkdir(dirPath, { recursive: true }, err => {
          if (err && err.code !== 'EEXIST') {
            console.error(`Error while creating directory '${dirPath}'.`);
          }

          const logsArray = [{ message, time: Date.now() }];
          const updatedLogsFileContent =
            JSON.stringify({ 'logs': logsArray }, null, JSON_SPACES_NUM);

          fs.writeFile(filePath, updatedLogsFileContent, 'utf8', err => {
            if (err) {
              console.error(`Error while writing to file with name '${filePath}'.`);
            }
          });
        });
      } else {
        console.error(`Error while reading file with name '${filePath}'.`);
      }

    } else {
      const logsArray = [...JSON.parse(content).logs, { message, time: Date.now() }];
      const updatedLogsFileContent =
        JSON.stringify({ 'logs': logsArray }, null, JSON_SPACES_NUM);

      fs.writeFile(filePath, updatedLogsFileContent, 'utf8', err => {
        if (err) {
          console.error(`Error while writing to file with name '${filePath}': ${err}`);
        }
      });
    }
  });
};

const handleResponse = (response, statusCode, logMessage,
                        content = logMessage,
                        contentType = 'text/html',
                        logFilePath = LOGS_FILE_PATH) => {
  response.writeHead(statusCode, { 'Content-type': contentType });
  response.end(content);
  logToFile(logMessage, logFilePath);
};

module.exports = () => {
  http.createServer((req, res) => {
    const { pathname, query } = url.parse(req.url, true);

    if (req.method === 'POST') {
      if (pathname === '/file') {
        if (query.filename && query.content) {
          fs.writeFile(`${FILES_DIR_PATH}/${query.filename}`, query.content, 'utf8', err => {
            if (err) {
              if (err.code === 'ENOENT') {
                fs.mkdir(FILES_DIR_PATH, { recursive: true }, err => {
                  if (err && err.code !== 'EEXIST') {
                    const logMessage = `Error while creating directory '${FILES_DIR_PATH}'.`;
                    logToFile(logMessage);
                  }

                  fs.writeFile(`${FILES_DIR_PATH}/${query.filename}`, query.content, 'utf8', err => {
                    if (err) {
                      const logMessage = `Error while writing to file with name '${query.filename}.`;
                      handleResponse(res, STATUS_CODE.BAD_REQUEST, logMessage);
                    }
                  });
                });
              } else {
                const logMessage = `Error while writing to file with name '${query.filename}.`;
                handleResponse(res, STATUS_CODE.BAD_REQUEST, logMessage);
                return;
              }
            }

            const logMessage = `New file with name '${query.filename}' saved.`;
            handleResponse(res, STATUS_CODE.OK, logMessage);
          });
        } else {
          const logMessage = `Incorrect query params. Please input query params 'filename' and 'content'.`;
          handleResponse(res, STATUS_CODE.BAD_REQUEST, logMessage);
        }

      } else {
        const logMessage = `Resource '${pathname}' not found.`;
        handleResponse(res, STATUS_CODE.NOT_FOUND, logMessage);
      }
    }

    if (req.method === 'GET') {
      let dirName;
      let fileName = '';

      if (pathname.includes('.')) {
        dirName = pathname.slice(0, pathname.lastIndexOf('/'));
        fileName = pathname.slice(pathname.lastIndexOf('/') + 1);
      } else {
        dirName = pathname;
      }

      if (dirName === '/file') {
        fs.readFile(`${FILES_DIR_PATH}/${fileName}`, 'utf8', (err, content) => {
          if (err) {
            const logMessage = `No files with name '${fileName}' found.`;
            handleResponse(res, STATUS_CODE.BAD_REQUEST, logMessage);
            return;
          }

          const logMessage = `File with name '${fileName}' has been read.`;
          handleResponse(res, STATUS_CODE.OK, logMessage, content);
        });

      } else if (pathname === '/logs') {
        const logFileName = LOGS_FILE_PATH.slice(LOGS_FILE_PATH.lastIndexOf('/') + 1);

        if (query.from || query.to) {
          fs.readFile(`${LOGS_FILE_PATH}`, 'utf8', (err, content) => {
            if (err) {
              const logMessage = `No files with name '${logFileName}' found.`;
              handleResponse(res, STATUS_CODE.BAD_REQUEST, logMessage);
              return;
            }

            const logsArray = [...JSON.parse(content).logs];
            const filteredLogsArray = logsArray.filter(el =>
              el.time >= (query.from ? +query.from : 0) &&
              el.time <= (query.to ? +query.to : Infinity)
            );
            const filteredContent =
              JSON.stringify({ 'logs': filteredLogsArray }, null, JSON_SPACES_NUM);
            const logMessage = `File with name '${logFileName}' has been read.`;
            handleResponse(res, STATUS_CODE.OK, logMessage, filteredContent, 'application/json');
          });
        } else {
          fs.readFile(`${LOGS_FILE_PATH}`, 'utf8', (err, content) => {
            if (err) {
              const logMessage = `No files with name '${logFileName}' found.`;
              handleResponse(res, STATUS_CODE.BAD_REQUEST, logMessage);
              return;
            }

            const logMessage = `File with name '${logFileName}' has been read.`;
            handleResponse(res, STATUS_CODE.OK, logMessage, content, 'application/json');
          });
        }

      } else {
        const logMessage = dirName === '/logs' && fileName
          ? `Incorrect request '${dirName}/${fileName}', please try '${dirName}'`
          : `Resource '${dirName}' not found.`;
        handleResponse(res, STATUS_CODE.NOT_FOUND, logMessage);
      }
    }
  }).listen(+process.env.PORT || PORT, () => console.log(`Server has started on port ${PORT}`));
}
